console.log("NO. 1")

// Code di sini
function range(startnum, finishnum){
    var rangearr = [];

    if (startnum > finishnum){
        var rangelength = startnum - finishnum + 1;
        for (var i  = 0; i< rangelength; i++ ){
            rangearr.push(startnum - i)
        }
    }else if (startnum < finishnum ){
        var rangelength = finishnum - startnum + 1;
        for (var i = 0; i<rangelength; i++){
            rangearr.push(startnum + i)
        }
    }else if (!startnum || !finishnum){
        return -1
    }
    return rangearr
} 

console.log(range(1, 10)) //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)) // -1
console.log(range(11,18)) // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)) // [54, 53, 52, 51, 50]
console.log(range()) // -1 

console.log("NO. 2")

// Code di sini
function rangeWithStep(startnum, finishnum, step){
    var rangearr = [];

    if (startnum > finishnum){
        var currentnum = startnum;
        for (var i = 0; currentnum >= finishnum; i++){
            rangearr.push(currentnum)
            currentnum -= step 
        }
    }else if (startnum < finishnum){
        var currentnum = startnum;
        for (var i = 0; currentnum <= finishnum; i++){
            rangearr.push(currentnum)
            currentnum += step
        }
    }else if (!startnum || !finishnum ||!step){
        return -1
    } 
    return rangearr
}
 
console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5]

console.log("NO. 3")

// Code di sini
function sum(startnum, finishnum, step){
    var rangearr = [];
    var distance;

    if (!step){
        distance = 1
    }else {
        distance = step
    }

    if (startnum > finishnum){
        var currentnum = startnum;
        for (var i = 0; currentnum >= finishnum; i++){
            rangearr.push(currentnum)
            currentnum -= distance
        }
    }else if (startnum < finishnum){
        var currentnum = startnum;
        for (var i = 0; currentnum <= finishnum; i++){
            rangearr.push(currentnum)
            currentnum += distance 
        }
    }else if (!startnum && !finishnum && !step){
        return 0

    }else if (startnum) {
        return startnum

    }
    var total = 0;
    for (var i =0; i< rangearr.length; i++ ){
        total = total + rangearr[i]
    }
    return total

}

console.log(sum(1,10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15,10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0

console.log("NO. 4")


//contoh input
var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
] 

function datahandling(data){
    var datalength = data.length
    for (var i = 0; i < datalength; i++){
        var id = "Nomor ID :" + data[i][0]
        var nama = " Nama Lengkap :" + data[i][1]
        var ttl = " TTL :" + data[i][2] + " " + data[i][3]
        var hobi = "Hobi :" + data[i][4]

        console.log(id)
        console.log(nama)
        console.log(ttl)
        console.log(hobi)

    }

}

console.log()
datahandling(input)

console.log("NO. 5")

// Code di sini
function balikKata(kata){
    var katabaru = " ";
    for (var i= kata.length - 1; i>=0; i--){
        katabaru += kata[i]
    }
    return katabaru;
}

console.log(balikKata("Kasur Rusak")) // kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers")) // srebnaS ma I 

console.log("NO. 6")

var input = ["0001", "Roman Alamsyah ", "Bandar Lampung", "21/05/1989", "Membaca"]
dataHandling2(input);

function dataHandling2(data){
    var newData = data
    var newName = data[1] + "Elsharawy"
    var newProvinsi = "Provinsi" + data[2]
    var gender = "Pria"
    var institusi = "SMA International Metro"

    newData.splice(1, 1, newName)
    newData.splice(2, 1, newProvinsi)
    newData.splice(4, 1, gender)

    var arrDate = data[3]
    var newDate = arrDate.split('/')
    var monthnum = newDate[1]
    var monthname = " "

    switch(monthname){
        case "01":
            monthname = "Januari"
            brake;
            case "02":
                monthname = "Februari"
                brake;
                case "03":
                    monthname = "Maret"
                    brake;
                    case "04":
                        monthname = "April"
                        brake;
                        case "05":
                            monthname = "Mei"
                            brake;
                            case "06":
                                monthname = "Juni"
                                brake;
                                case "07":
                                    monthname = "Juli"
                                    brake;
                                    case "08":
                                        monthname = "Agustus"
                                        brake;
                                        case "09":
                                            monthname = "September"
                                            brake;
                                            case "10":
                                                monthname = "Oktober"
                                                brake;
                                                case "11":
                                                    monthname = "November"
                                                    brake;
                                                    case "12":
                                                        monthname = "Desember"
                                                        brake;
                                                        default:
                                                            break;
                                                                                                                                                                                                                                                                                                                                    
                                 
    }
    var dateJoin = newDate.join("-")
    var dateArr = newDate.sort(function(value1, value2){
        value2- value1
    })
    var editName = newName.slice(0,15)
    console.log(newData)

    console.log(monthname)
    console.log(dateArr)
    console.log(dateJoin)
    console.log(editName)
}