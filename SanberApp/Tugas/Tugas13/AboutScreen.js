import React from 'react'
import { StyleSheet, 
            Text, 
            View,
            ScrollView,
            Image,
            TextInput,
            Button
            
        } from 'react-native'
import { color } from 'react-native/Libraries/Components/View/ReactNativeStyleAttributes'
const AboutScreen = () => {
    return(
        <ScrollView>
            
            <View style = {styles.container}>
                
                <Text style={styles.name}>Tentang saya</Text>
                <Text style={styles.name}>Gambar Profil</Text>
                <Text style={styles.name}>Daffa Nugraha</Text>
                <Text style={styles.kerjaan}>Tukang ketik</Text>
                
                <View style={styles.kotak}>
                    <Text style={styles.juduldalam}>Portofolio</Text>
                    <View style={styles.kotakdalam}>
                        <View>
                            
                            <Text style={styles.name}>Gambar GitLab</Text>
                            <Text style={styles.textdalam}>@daffa21</Text>
                        </View>
                        <View>
                            <Text style={styles.name}>Gambar GitHub</Text>
                            <Text style={styles.textdalam}>@daffa21</Text>
                        </View>
                    </View>
                </View>
                
                <View style={styles.kotak}>
                    <Text style={styles.juduldalam}>Hubungi Saya</Text>
                    <View style={styles.kotakdalamver}>
                        <View style={styles.kotakdalamverhub}>
                            <View>
                                <Text style={styles.name}>Gambar Facebook</Text>
                            </View>
                            <View>
                                <Text style={styles.textdalam}>@daffa21</Text>
                            </View>
                        </View>
                        <View style={styles.kotakdalamverhub}>
                            <View>
                                <Text style={styles.name}>Gambar Instagram</Text>
                            </View>
                            <View>
                                <Text style={styles.textdalam}>@daffa21</Text>
                            </View>
                        </View>
                        <View style={styles.kotakdalamverhub}>
                            <View>
                                <Text style={styles.name}>Gambar Twitter</Text>
                            </View>
                            <View>
                                <Text style={styles.textdalam}>@daffa21</Text>
                            </View>
                
                        </View>

                    </View>

                </View>
            </View>
        </ScrollView>
    )
}

export default AboutScreen

const styles = StyleSheet.create({
    container:{
        marginTop:64
    },
    judul:{
        fontSize:36,
        fontWeight:'bold',
        color:'#003366',
        textAlign:'center'
    },
    icon:{
        textAlign:'center'
    },
    name:{
        fontSize:24,
        fontWeight:'bold',
        color:'#003366',
        textAlign:'center'
    },
    kerjaan:{
        fontSize:16,
        fontWeight:'bold',
        color:'#3EC6FF',
        textAlign:'center',
        marginBottom:7
    },
    kotak:{
        borderColor:'blue',
        borderRadius:10,
        borderColor:'#000',
        padding:5,
        backgroundColor:'#EFEFEF',
        marginBottom:9
    },
    kotakdalam:{
        borderTopWidth:2,
        borderTopColor:'#003366',
        flexDirection:'row',
        justifyContent:'space-around'
    },
    kotakdalamverhub:{
        borderTopWidth:2,
        borderTopColor:'#003366',
        flexDirection:'column',
        justifyContent:'space-around'

    },
    kotakdalamverhub:{
        height:50,
        flexDirection:'row',
        justifyContent:'center',
        marginBottom:2
    },
    juduldalam:{
        fontSize:18,
        color:'#003366'
    },
    textdalam:{
        fontSize:16,
        fontWeight:'bold',
        color:'#003366',
        textAlign:'center'
    },
    input:{
        height:40,
        borderColor:'grey',
        borderWidth:1
    }
})