import React from 'react'
import { Platform, 
        Image, 
        ScrollView, 
        StyleSheet, 
        Text, 
        View, 
        TextInput, 
        TouchableOpacity, 
        Button, 
        KeyboardAvoidingView} from 'react-native'
import { color } from 'react-native/Libraries/Components/View/ReactNativeStyleAttributes'
import sizesDiffer from 'react-native/Libraries/Utilities/differ/sizesDiffer'
//import { TextInput } from 'react-native-web'

const LoginScreen = () => {
    return(
        <KeyboardAvoidingView
        behavior={Platform.OS == "ios" ? "padding" : "height"}
        style={styles.container}
        >
        <ScrollView>
            <View style={styles.containerView}>
            <Image source={require('../Tugas13/assets/logo_putih.png')}/>
                <Text style={styles.logintext}>Login</Text>
                <View style={styles.formInput}>
                    <Text style={styles.formtext}>Username</Text>
                    <TextInput style={styles.input}/>
                </View>
                <View style={styles.formInput}>
                    <Text style={styles.formtext}>Password</Text>
                    <TextInput style={styles.input} secureTextEntry = {true}/>
                </View>
                <View style={styles.kotaklogin}>
                    <TouchableOpacity style={styles.btlogin}>
                        <Text style={styles.textbt}>Masuk</Text>
                    </TouchableOpacity>
                    <Text style={styles.autotext}>Atau</Text>
                    <TouchableOpacity style={styles.btreg}>
                        <Text style={styles.textbt}>Daftar</Text>
                    </TouchableOpacity>
                </View>
            </View>
        </ScrollView>
        </KeyboardAvoidingView>
    )
}

export default LoginScreen

const styles = StyleSheet.create({
    container:{
        flex:1,
        marginTop:60,
    }, 
    logintext:{
        fontSize: 36,
        marginTop: 30,
        textAlign:'center',
        color:"#003366",
        marginVertical:10
    }, 
    formtext:{
        color:"#003366"
    },
    autotext:{
        fontSize:24,
        color:"#3ec6ff",
        textAlign:'center'
    },
    formInput:{
        marginHorizontal:30,
        marginVertical:5,
        alignContent:"center",
        width:300
    },
    input:{
        height:40,
        borderColor:"black",
        padding:10,
        borderWidth:1
    },
    btlogin:{
        alignItems:'center',
        backgroundColor:'#3EC6FF',
        padding:10,
        borderRadius:16,
        marginHorizontal:30,
        marginBottom:10,
        width:140,
        height:40,
        left:70,
        marginTop:10
    },
    btreg:{
        alignItems:'center',
        backgroundColor:'#003366',
        padding:10,
        borderRadius:16,
        marginHorizontal:30,
        marginBottom:10,
        width:140,
        height:40,
        left:70,
        marginTop:10,
    }

})