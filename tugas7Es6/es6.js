console.log("Nomor 1")
console.log("Merubah FUnction ke arrow function")
const golden = () => {
    console.log("this is golden!!")
  }
   
  golden()

  console.log("Nomor 2")
  console.log("Object Literal")

  const newFunction = (fristname, lastname) => {
      return{
          fristname, lastname,
          fullname(){
              console.log(fristname + " " + lastname)
              return
          }
      }
  }
  newFunction("Willianm", "Imoh").fullname() 

  console.log("Nomor 3")

  const newObject = {
    firstName: "Harry",
    lastName: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
  }

  const {firstName, lastName, destination, occupation, spell} = newObject

  console.log(firstName, lastName, destination, occupation, spell)

console.log("Nomor 4")

const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]

const combined = [...west,...east]
//Driver Code
console.log(combined)

console.log("Nomor 5")
console.log("Template Literal")

const planet = "earth"
const view = "glass"
var before = `Lorem ${view} dolor sit amet,  
    consectetur adipiscing elit,  ${planet} do eiusmod tempor
    incididunt ut labore et dolore magna aliqua. Ut enim
    ad minim veniam`
 
// Driver Code
console.log(before) 